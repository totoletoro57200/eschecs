
unit FormAbout;

{.$IFDEF UNIX}
{.$DEFINE USECTHREADS}
{.$ENDIF}

interface

uses
{.$IFDEF UNIX}
  //CThreads,
  //CWString,
{.$ENDIF}
  SysUtils,
  Classes,
  Math,
  
  fpg_base,
  fpg_button,
  fpg_form,
  fpg_hyperlink,
  fpg_label,
  fpg_main,
  
  Language;

procedure ShowFormAbout(const AAppName, ADescription, ATitle, AButton, AHomePage: string);

implementation

{$I icon} 

type
  TFormAbout = class(TfpgForm)
  public
    BTOk: TfpgButton;
    LBAppName: TfpgLabel;
    LBDescription: TfpgLabel;
    HLHomePage: TfpgHyperlink;
    procedure AfterCreate; override;
    procedure CloseMsg(Sender: TObject);
  end;

procedure ShowFormAbout(const AAppName, ADescription, ATitle, AButton, AHomePage: string);
var
  LForm : TFormAbout;
begin
  fpgApplication.CreateForm(TFormAbout, LForm);
  with LForm do
  try
    BTOk.Text := AButton;
    LBAppName.Text := AAppName;
    LBDescription.Text := ADescription;
    WindowTitle := ATitle;
    if AHomePage <> EmptyStr then
    begin
      HLHomePage.Visible := TRUE;
      HLHomePage.Text := AHomePage;
    end;
    ShowModal;
  finally
    Free;
  end;
end;

procedure TFormAbout.AfterCreate;
begin
  Name := 'FormAbout';
  SetPosition(0, 0, 400, 148);
  WindowTitle := '';
  IconName := 'vfd.eschecs';
  BackgroundColor := $80000001;
  Sizeable := FALSE;
  Visible := FALSE;
  Hint := '';
  WindowPosition := wpOneThirdDown;

  LBAppName := TfpgLabel.Create(self);
  with LBAppName do
  begin
    Name := 'LBAppName';
    SetPosition(8, 16, 384, 18);
    Alignment := taCenter;
    AutoSize := FALSE;
    FontDesc := 'Liberation Sans-10:antialias=TRUE:bold';
    ParentShowHint := FALSE;
    Text := '';
    Hint := '';
  end;
  
  LBDescription := TfpgLabel.Create(self);
  with LBDescription do
  begin
    Name := 'LBDescription';
    SetPosition(8, 48, 384, 20);
    Alignment := taCenter;
    AutoSize := FALSE;
    FontDesc := 'Liberation Sans-10:antialias=TRUE';
    ParentShowHint := FALSE;
    Text := '';
    Hint := '';
  end;
  
  HLHomePage := TfpgHyperlink.Create(self);
  with HLHomePage do
  begin
    Name := 'HLHomePage';
    SetPosition(8, 80, 384, 20);
    Alignment := taCenter;
    AutoSize := FALSE;
    FontDesc := 'Liberation Sans-10:antialias=TRUE:bold';
    HotTrackColor := TfpgColor($B1004001);
    HotTrackFont := 'Liberation Sans-10:antialias=TRUE:underline:bold';
    ParentShowHint := FALSE;
    Text := '';
    TextColor := TfpgColor($B1004001);
    URL := 'https://gitlab.com/rchastain/eschecs';
    Visible := FALSE;
    Hint := '';
  end;
  
  BTOk := TfpgButton.Create(self);
  with BTOk do
  begin
    Name := 'BTOk';
    SetPosition(312, 112, 80, 28);
    Text := 'Close';
    FontDesc := '#LBAppName';
    ImageName := '';
    ModalResult := mrOK;
    ParentShowHint := FALSE;
    TabOrder := 1;
  end;
end;

procedure TFormAbout.CloseMsg(Sender: TObject);
begin
  Close;
end;

end.
