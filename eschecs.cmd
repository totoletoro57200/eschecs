
REM Delete old configuration files

del /q config\eschecs.ini
del /q config\history.fen

REM Delete old files

del /q *.err
del /q *.log
del /q *.out
del /q *.pgn

REM Start Eschecs

eschecs.exe engines\cheng\441\cheng4.exe 2> eschecs.err | tee.exe eschecs.out 

REM See eschecs.sh for command line examples.
