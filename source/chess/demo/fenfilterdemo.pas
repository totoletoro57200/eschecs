
uses
  SysUtils, FenFilter, FenExamples;

var
  f: TFenFilter;
  i: integer;
  
begin
  f := TFenFilter.Create;
  
  (*
  for i := Low(CFenExamples) to High(CFenExamples) do
    WriteLn(f.IsFen(CFenExamples[i]));
  *)
   
  for i := Low(CFenDestructionExamples) to High(CFenDestructionExamples) do
    WriteLn(Format('%2d ', [i]), f.IsFen(CFenDestructionExamples[i]));
  
  f.Free;
end.
