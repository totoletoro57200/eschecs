## Create desktop launcher

## Script directory
D1="$(dirname "$(readlink -f "$0")")"

## Destination directories
D2="$HOME/Desktop"
D3="/usr/share/applications"

if [ ! -d $D2 ]; then
  D2="$HOME/Bureau"
fi

if [ ! -d $D2 ]; then
  D2=$(xdg-user-dir DESKTOP)
fi

if [ -d $D2 ] ;
then
  FILE=$D2/eschecs.desktop
  echo "Creating desktop launcher $FILE"
  cat > $FILE << EOF
[Desktop Entry]
Version=1.0
Type=Application
Name=Eschecs
Comment=Jeu d'échecs
Exec=$D1/eschecs %F
Icon=$D1/eschecs.ico
Path=$D1
Terminal=false
StartupNotify=true
Categories=Game;BoardGame;
EOF
  echo "Make launcher executable"
  sudo chmod -R 777 $FILE
  if [ -d $D3 ] ;
  then
    FILE2=$D3/eschecs.desktop
    echo "Copy $FILE to $FILE2"
    sudo cp -f $FILE $FILE2
  else
    echo "Cannot find directory $D3"
  fi
  echo "Done"
else
  echo "Cannot find directory $D2"
fi
