
# Engines

| Engine | Version | Logo | Author | Protocol | Code source | FRC support | Link |
| --- | --- | --- | --- | --- | --- | --- | --- |
| Alouette | 0.1.6 | ![alt text](images/logos/alouette/logo.png) | Roland Chastain | UCI | Pascal | Yes | [Website](https://gitlab.com/rchastain/alouette) |
| Arasan | 11.5 | ![alt text](images/logos/arasan/arasan.jpg) | Jon Dart | UCI | C++ | No | [Website](https://www.arasanchess.org/index.shtml) |
| Cheese | 3.1.1 | ![alt text](images/logos/cheese/cheese.png) | Patrice Duhamel | UCI | C++ | Yes | [Website](http://cheesechess.free.fr/en/index.html) |
| Cheng4 | 0.41 | ![alt text](images/logos/cheng/logo.png) | Martin Sedlak | UCI | C++ | Yes | [Website](http://www.vlasak.biz/cheng/) |
| CT800 | 1.42 | ![alt text](images/logos/ct800/CT800_V1.34_x32.png) | Rasmus Althoff | UCI | C | No | [Website](https://www.ct800.net/) |
| Dumb | 1.11 |  | Richard Delorme | UCI | D | Yes | [Website](https://github.com/abulmo/Dumb) |
| Durandal | 0.1.0 | ![alt text](images/logos/durandal/durandal_100x50.png) | Roland Chastain | UCI | Pascal | Yes | [Website](https://gitlab.com/rchastain/durandal) |
| Floyd | 0.9 | ![alt text](images/logos/floyd/floyd-logo.png) | Marcel van Kervinck | UCI | C | No | [Website](https://marcelk.net/floyd/) |
| FracTal | 1.0 |  | Visan Alexandru | UCI | C++ | No | [Website](https://github.com/visanalexandru/FracTal-ChessEngine) |
| Fridolin | 3.10 | ![alt text](images/logos/fridolin/Fridolin.jpg) | Christian Sommerfeld | UCI | C++ | Yes | [Website](https://sites.google.com/site/fridolinchess/) |
| Fruit | 2.1 | ![alt text](images/logos/fruit/fruit-logo_100x50.jpg) | Fabien Letouzey | UCI | C++ | No | [Website](https://www.fruitchess.com) |
| Galjoen | 0.40 | ![alt text](images/logos/galjoen/galjoen_100x50.png) | Werner Taelemans | UCI | C++ | Yes | [Website](http://www.goudengaljoen.be/) |
| Hermann | 2.8 | ![alt text](images/logos/hermann/hermann-2.jpg) | Volker Annuss | UCI | - | Yes | [Website](http://www.nnuss.de/Hermann/) |
| KnightX | 2.9.1 | ![alt text](images/logos/knightx/knightx_100x50.png) | Christophe Jolly | UCI | - | No | [Website](http://technochess.free.fr) |
| La Dame Blanche | 2.0 | ![alt text](images/logos/ladameblanche/ladameblanche.gif) | Marc-Philippe Huget | XBoard | - | No | [Website](http://www.quarkchess.de/ladameblanche/) |
| LittleWing | 0.6.0 | ![alt text](images/logos/littlewing/littlewing.gif) | Vincent Ollivier | UCI | Rust | No | [Website](https://vinc.cc/projects/littlewing/) |
| Luciole | 0.0.6 | ![alt text](images/logos/luciole/luciole_100x50.jpg) | Roland Chastain | UCI | Lua | Yes | [Website](https://gitlab.com/rchastain/luciole) |
| Moustique | 0.4.1 | ![alt text](images/logos/moustique/Farman-F455-Moustique.png) | Jürgen Schlottke, Roland Chastain | UCI | Pascal | No | [Website](https://gitlab.com/rchastain/moustique) |
| N.E.G. | 1.2 | ![alt text](images/logos/neg/neg.gif) | Harm Geert Muller | XBoard | C | No | [Website](https://home.hccnet.nl/h.g.muller/chess.html) |
| OpenTal | 1.0 | ![alt text](images/logos/opental/rodent2.jpg) | Pawel Koziol | UCI | C++ | No | [Website](http://www.pkoziol.cal24.pl/opental/) |
| Pharaon | 3.5.1 | ![alt text](images/logos/pharaon/Pharaon3.png) | Frank Zibi | UCI | - | Yes | [Website](http://www.fzibi.com/pharaon.htm) |
| Sapeli | 1.53 | ![alt text](images/logos/sapeli/logo.jpg) | Toni Helminen | UCI | C | Yes | [Website](https://github.com/SamuraiDangyo/Sapeli) |
| Senpai | 2.0 | ![alt text](images/logos/senpai/senpai2_2_102.png) | Fabien Letouzey | UCI | C++ | Yes | [Website](http://www.amateurschach.de/main/_senpai.htm) |
| SlowChess | 2.2 | ![alt text](images/logos/slowchess/slowchess_100x50.png) | Jonathan Kreuzer | UCI | C++ | Yes | [Website](https://www.3dkingdoms.com/chess/slow.htm) |
| Zappa | 1.1 | ![alt text](images/logos/zappa/zappa_logo_handel_100x50.jpg) | Anthony Cozzie | UCI | - | No | [Website](https://www.acoz.net/zappa/) |
